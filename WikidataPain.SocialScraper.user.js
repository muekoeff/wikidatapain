// ==UserScript==
// @author       µKöff
// @grant        GM_notification
// @grant        GM_registerMenuCommand
// @grant        GM_setClipboard
// @grant        GM_xmlhttpRequest
// @homepageURL  https://gitlab.com/muekoeff/wikidatapain/
// @icon         https://www.wikidata.org/static/favicon/wikidata.ico
// @match        http*://*/*
// @name         WikidataPain: SocialScraper
// @namespace    https://muekev.de/
// @version      $VERSION
// ==/UserScript==

( function () {
	let muekoeffDatascraperInjected = false;

	function datascraper() {
		if ( window.self !== window.top ) {
			return; // Not in frames
		}
		if ( muekoeffDatascraperInjected !== false ) {
			return; // Not if already injected
		} else {
			muekoeffDatascraperInjected = true;
		}

		let results = [];
		const body = document.body.innerHTML;

		// Facebook
		for ( const match of Array.from( body.matchAll( /https?:\/\/(?:.+\.)?facebook\.com\/(.*?)\/?\/?["' ?]/gi ) ) ) {
			if ( /^(hashtag|legal|post|privacy|sharer)/.test( decodeURIComponent( match[ 1 ] ) ) ) {
				continue;
			}
			results.push( [ 'P2013', decodeURIComponent( match[ 1 ] ), 'Facebook' ] );
		}
		// Imprint
		/**
		 * @type {NodeListOf<HTMLAnchorElement>}
		 */
		const imprintLink = document.querySelectorAll( 'a[href]' );
		if ( imprintLink !== null && imprintLink.length > 0 ) {
			const imprintLinkList = [ ...Array.from( imprintLink ) ].find( ( link ) => /impressum|imprint/i.test( link.text ) || /impressum|imprint/i.test( link.href ) );

			if ( imprintLink !== null ) {
				console.log( imprintLink );
				GM_xmlhttpRequest( {
					method: 'GET',
					url: imprintLinkList.href,
					onload: function ( response ) {
						console.debug( '[WikidataPain.SocialScraper] ', response.responseText );
					}
				} );
			}
		}
		// Instagram
		for ( const match of Array.from( body.matchAll( /https?:\/\/(?:.+\.)?instagram\.com\/(.*?)\/?\/?["' ?]/gi ) ) ) {
			if ( /^(legal|p)/.test( decodeURIComponent( match[ 1 ] ) ) ) {
				continue;
			}
			results.push( [ 'P2003', decodeURIComponent( match[ 1 ] ), 'Instagram' ] );
		}
		// LinkedIn
		for ( const match of Array.from( body.matchAll( /https?:\/\/(?:.+\.)?linkedin\.com\/company\/(.*?)\/?["' ?]/gi ) ) ) {
			results.push( [ 'P4264', decodeURIComponent( match[ 1 ] ), 'LinkedIn' ] );
		}
		// Twitch
		for ( const match of Array.from( body.matchAll( /https?:\/\/(?:.+\.)?twitch\.tv\/(.*?)\/?["' ?]/gi ) ) ) {
			results.push( [ 'P5797', decodeURIComponent( match[ 1 ] ), 'Twitch' ] );
		}
		// Twitter
		for ( const match of Array.from( body.matchAll( /https?:\/\/(?:.+\.)?twitter\.com\/(.*?)\/?["' ?]/gi ) ) ) {
			if ( /^(intent|privacy|share|widgets\.js)/.test( decodeURIComponent( match[ 1 ] ) ) ) {
				continue;
			}
			results.push( [ 'P2002', decodeURIComponent( match[ 1 ] ), 'Twitter' ] );
		}
		// Vimeo
		for ( const match of Array.from( body.matchAll( /https?:\/\/(?:.+\.)?vimeo\.com\/(.*?)\/?["' ?]/gi ) ) ) {
			results.push( [ 'P4015', decodeURIComponent( match[ 1 ] ), 'Vimeo' ] );
		}
		// Xing
		for ( const match of Array.from( body.matchAll( /https?:\/\/(?:.+\.)?xing\.(?:com|de)\/(?:compan(?:ies|y)|pages)\/(.*?)\/?["' ?]/gi ) ) ) {
			results.push( [ 'P6619', decodeURIComponent( match[ 1 ] ), 'Xing' ] );
		}
		// YouTube
		for ( const match of Array.from( body.matchAll( /https?:\/\/(?:.+\.)?youtube\.com\/channel\/(.*?)\/?["' ?]/gi ) ) ) {
			results.push( [ 'P2397', decodeURIComponent( match[ 1 ] ), 'YouTube' ] );
		}

		// Unique
		results = unique( results );

		// Output
		if ( results.length > 0 ) {
			const out = results.map( ( [ property, value, _label ] ) => {
				return {
					action: 'wbsetclaim',
					config: {
						allowCreate: true,
						entity: 'current',
						method: 'refine'
					},
					claim: {
						type: 'statement',
						mainsnak: unsafeWindow.Pain.Snak.fromString( property, value ),
						rank: 'normal'
					}
				};
			} );

			GM_setClipboard( `[\n${out.map( ( item ) => JSON.stringify( item ) ).join( ',\n' )}\n]` );

			const check = results.map( ( [ _property, value, label ] ) => `${label}: ${value}` );
			GM_notification( check.join( '\n' ), 'Scraper: Social' );
		} else {
			GM_notification( 'No results', 'Scraper: Social' );
		}
	}

	function unique( arr ) {
		const set = new Set( arr.map( JSON.stringify ) );
		const out = [];
		set.forEach( ( item ) => out.push( JSON.parse( item ) ) );
		return out;
	}

	GM_registerMenuCommand( 'Scrape this website', datascraper );
}() );
