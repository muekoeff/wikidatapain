# Pain for Wikidata
**Pain for Wikidata** is JavaScript-userscript "framework" written by [nw520](https://wikidata.org/wiki/User:Nw520) for their personal use with the goal of providing some utilities for otherwise quite painful systematic edits to Wikidata for which QuickStatements are not powerful enough. Functions provided are rather low-level and very close to the original MediaWiki actions API of Wikidata and therefore require some knowledge on it.

Please also note that **this project is not intended for productive use** but rather only available on GitLab for providing the author with a compiled and easily accessible [documentation](https://muekoeff.gitlab.io/wikidatapain/docs/jsdoc/) and to make their edits more transparent. While others are of course free to use parts of this project I highly discourage directly using the whole "framework" as there'll probably be breaking changes in the future (and I won't care about backwards-compatibility).
Additionally quite a lot of methods haven't been properly written yet since the need for that didn't arise yet.


[UserScript](https://muekoeff.gitlab.io/wikidatapain/WikidataPain.user.js) • [SocialScraper](https://muekoeff.gitlab.io/wikidatapain/WikidataPain.SocialScraper.user.js) • [Documentation](https://muekoeff.gitlab.io/wikidatapain/docs/jsdoc/)
