declare function GM_notification( text: string, title?: string, image?: string, onclick?: Function ): void;
declare function GM_registerMenuCommand( caption: string?, onClick: Function ): void;
declare function GM_setClipboard( data?: string, type?: string ): void;
declare function GM_xmlhttpRequest( details: Object ): void;
declare var unsafeWindow: { OO: any, mw: any, Pain: any, WikidataMandat: any };
